package cz.vse.weatherapp.domain.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true) //  if the JSON response has any fields that we don't have in our WeatherResponse class, they will simply be ignored.
@Data
public class WeatherResponse {

    private List<Weather> weather;

    @Data
    public static class Weather {
        private String description;

    }

}
