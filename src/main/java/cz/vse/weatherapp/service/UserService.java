package cz.vse.weatherapp.service;

import cz.vse.weatherapp.domain.jpa.AppUser;
import cz.vse.weatherapp.domain.jpa.Destination;
import cz.vse.weatherapp.domain.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WeatherService weatherService;

    public AppUser getOrAddUser(String name) {
        AppUser user = userRepository.findByUserName(name);
        if (user == null) {
            user = createUserWithInitialDestinations(name);
            userRepository.save(user);
        }
        return user;
    }

    private AppUser createUserWithInitialDestinations(String userName) {
        AppUser user = new AppUser();
        user.setUserName(userName);

        List<Destination> destinations = user.getDestinations();
        if (destinations == null) {
            destinations = new ArrayList<>();
            Destination prague = new Destination();
            prague.setName("Prague");
            Destination berlin = new Destination();
            berlin.setName("Berlin");
            Destination paris = new Destination();
            paris.setName("Paris");
            destinations.add(prague);
            destinations.add(berlin);
            destinations.add(paris);
            for (Destination destination : destinations) {
                destination.setWeather(weatherService.getCurrentWeather(destination.getName()));
            }
            user.setDestinations(destinations);
        } else {
            // todo
        }

        userRepository.save(user);
        return user;
    }

    public AppUser addDestination(String userName, String cityName) {
        AppUser user = userRepository.findByUserName(userName);
        if (user != null && cityName != null) {
            Destination destination = new Destination();
            destination.setName(cityName);
            destination.setWeather(weatherService.getCurrentWeather(cityName));
            user.getDestinations().add(destination);
            userRepository.save(user);
        }
        return user;
    }

    // todo remove destination


    public List<Destination> fetchAllTimezones(List<Destination> cities) {
        RestTemplate restTemplate = new RestTemplate();

        for (Destination city : cities) {
            // Fetch the timezone
            String timezone = restTemplate.getForObject("https://timezone-api-url.com?city=" + city.getName(), String.class);

            // Set the timezone
            city.setTimezone(timezone);
        }

        return cities;
    }


    public AppUser removeDestination(String userName, String cityName) {
        AppUser user = userRepository.findByUserName(userName);
        for (Destination destination : user.getDestinations()) {
            if (destination.getName().equals(cityName)) {
                user.getDestinations().remove(destination);
                break;
            }
        }
        userRepository.save(user);
        return user;
    }

    public AppUser mergeAllDestinations(String userName) {
        List<AppUser> users = userRepository.findAll();
        Set<Destination> mergedDestinations = new HashSet<>();
        for (AppUser user : users) {
            for (Destination destination : user.getDestinations()) {
                String name = destination.getName();
                boolean found = false;
                for (Destination mergedDestination : mergedDestinations) {
                    if (mergedDestination.getName().equals(name)) {
                        found = true;
                    }
                }
                if (!found) {
                    mergedDestinations.add(destination);
                }
            }
        }
        AppUser currentUser = userRepository.findByUserName(userName);
        currentUser.getDestinations().clear();
        currentUser.getDestinations().addAll(mergedDestinations);
        return currentUser;
    }
}
