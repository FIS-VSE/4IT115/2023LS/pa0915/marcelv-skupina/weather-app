package cz.vse.weatherapp.controller;

import cz.vse.weatherapp.domain.jpa.AppUser;
import cz.vse.weatherapp.domain.jpa.Destination;
import cz.vse.weatherapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.http.HttpClient;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/destination")
public class DestinationController {

    @Autowired
    private UserService userService;

    @RequestMapping("/list")
    public String list(@RequestParam String userName, Model model) {
        AppUser user = userService.getOrAddUser(userName);
        model.addAttribute("user", user);
        return "destination/list";
    }

    @RequestMapping("/add")
    public String add(@RequestParam String userName, @RequestParam(required = false) String cityName, Model model) {
        AppUser user = userService.addDestination(userName, cityName);
        model.addAttribute("user", user);
        return "destination/add";
    }

    @RequestMapping("/remove")
    public String remove(@RequestParam String userName, @RequestParam(required = false) String cityName, Model model) {
        AppUser user = userService.removeDestination(userName, cityName);
        model.addAttribute("user", user);
        return "destination/add";
    }

    @RequestMapping("/merge")
    public String merge(@RequestParam String userName, Model model) {
        AppUser user = userService.mergeAllDestinations(userName);
        model.addAttribute("user", user);
        return "destination/add";
    }

    @GetMapping("/timezones")
    public String fetchAllTimezones(@RequestParam(required = false) String userName, Model model) {
        // Get the user
        AppUser user = userService.getOrAddUser(userName);

        // Fetch all timezones
        List<Destination> citiesWithTimezones = userService.fetchAllTimezones(user.getDestinations());

        // Add the cities with timezones to the model
        model.addAttribute("citiesWithTimezones", citiesWithTimezones);

        // Redirect back to the page
        return "redirect:/destination/add?userName=" + userName;
    }


}
